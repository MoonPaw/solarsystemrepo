using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private float scrollSpeed;
    private float horizontalInput;
    private float verticalInput;
    private float mouseInput;
    private Rigidbody body;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        mouseInput = Input.GetAxis("Mouse ScrollWheel");

        if (mouseInput > 0f) // forward
        {

            body.velocity = new Vector3(0, mouseInput * scrollSpeed, 0);
        }
        if (mouseInput < 0f) // backwards
        {
            body.velocity = new Vector3(0, mouseInput * scrollSpeed, 0);
        }
        if (horizontalInput != 0f || verticalInput != 0f)
        {
          body.velocity = new Vector3(verticalInput * speed, 0 , -horizontalInput * speed);
        }
        else if(mouseInput == 0)
        {
            body.velocity = Vector3.zero;
        }



    }
}
